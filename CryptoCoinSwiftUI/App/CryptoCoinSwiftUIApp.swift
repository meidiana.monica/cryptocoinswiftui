//
//  CryptoCoinSwiftUIApp.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 24/11/22.
//

import SwiftUI

@main
struct CryptoCoinSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}

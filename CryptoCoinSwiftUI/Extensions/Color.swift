//
//  Color.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 20/12/22.
//

import Foundation
import SwiftUI

extension Color {
    static let theme = ColorTheme()
}

struct ColorTheme {
    let primaryTextColor = Color("PrimaryTextColor")
    let itemBackgroundColor = Color("ItemBackgroundColor")
}

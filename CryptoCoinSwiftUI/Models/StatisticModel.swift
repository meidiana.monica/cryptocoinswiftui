//
//  StatisticModel.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 20/12/22.
//

import Foundation

struct StatisticModel: Identifiable {
    let id = UUID().uuidString
    let title: String
    let value: String
    let percentageChange: Double?
}

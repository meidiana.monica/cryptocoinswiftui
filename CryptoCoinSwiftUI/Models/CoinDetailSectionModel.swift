//
//  CoinDetailSectionModel.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 20/12/22.
//

import Foundation

struct CoinDetailSectionModel {
    let title: String
    let stats: [StatisticModel]
}

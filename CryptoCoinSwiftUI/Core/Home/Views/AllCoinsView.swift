//
//  AllCoinsView.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 24/11/22.
//

import SwiftUI

struct AllCoinsView: View {
//    Notes : his code will make some trouble, because the HomeViewModel will called 2 times
//    @StateObject var homeVM = HomeViewModel()
//    ------------------------------------------------

//    this is the right code, the HomeViewModel will not be called 2 times. but we have to pass the homeVM from previous page i.e HomeView
    @StateObject var homeVM : HomeViewModel
//    ------------------------------------------------
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("All Coins")
                .font(.headline)
                .padding()
            
            HStack {
                Text("Coin")
                
                Spacer()
                
                Text("Price")
            }
            .foregroundColor(.gray)
            .font(.caption)
            .padding(.horizontal)
            
            ScrollView {
                VStack {
                    ForEach(homeVM.coins) { coin in
                        NavigationLink {
                            CoinDetailsView(coin: coin)
                        } label: {
                            CoinRowView(coin: coin)
                        }
                    }
                }
            }
        }
    }
}

//struct AllCoinsView_Previews: PreviewProvider {
//    static var previews: some View {
//        AllCoinsView()
//    }
//}

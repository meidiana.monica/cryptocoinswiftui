//
//  HomeView.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 24/11/22.
//

import SwiftUI

struct HomeView: View {
    @StateObject var homeVM = HomeViewModel()
//    @ObservedObject var homeVM = HomeViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                //Top movers view
                TopMoversView(homeVM: homeVM )
                
                Divider()
                
                //all coin view
                AllCoinsView(homeVM: homeVM)
            }
            .navigationTitle("Live Prices")
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

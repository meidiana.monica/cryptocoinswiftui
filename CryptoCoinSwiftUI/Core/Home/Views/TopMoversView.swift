//
//  TopMoversView.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 24/11/22.
//

import SwiftUI

struct TopMoversView: View {
    @StateObject var homeVM : HomeViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Top Movers")
                .font(.headline)
            
            ScrollView(.horizontal) {
                HStack(spacing: 16) {
                    ForEach(homeVM.topMovingCoins) { coin in
                        NavigationLink {
                            CoinDetailsView(coin: coin)
                        } label: {
                            TopMoversItemView(coin: coin)                            
                        }
                    }
                }
            }
        }.padding()
    }
}

struct TopMoversView_Previews: PreviewProvider {
    static var previews: some View {
        TopMoversView(homeVM: HomeViewModel())
    }
}

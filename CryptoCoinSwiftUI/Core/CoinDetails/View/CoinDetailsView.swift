//
//  CoinDetailsView.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 20/12/22.
//

import SwiftUI

struct CoinDetailsView: View {
    let viewModel: CoinDetailsViewModel
    
    init(coin: Coin) {
        self.viewModel = CoinDetailsViewModel(coin: coin)
    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                //MARK: Chart
                
                //MARK: overview
                CoinDetailsSection(model: viewModel.overviewSectionModel)
                    .padding(.vertical)
                
                //MARK: Additional details
                CoinDetailsSection(model: viewModel.additionalDetailsSectionModel)
                    .padding(.vertical)
            }
            .padding()
            .navigationTitle("Bitcoin")
        }
    }
}

struct CoinDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CoinDetailsView(coin: dev.coin)
    }
}

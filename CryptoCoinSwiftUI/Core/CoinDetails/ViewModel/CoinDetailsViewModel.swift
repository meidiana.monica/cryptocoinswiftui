//
//  CoinDetailsViewModel.swift
//  CryptoCoinSwiftUI
//
//  Created by Meidiana Monica on 21/12/22.
//

import Foundation

class CoinDetailsViewModel {
    private let coin: Coin
    
    var overviewSectionModel: CoinDetailSectionModel {
        //        price stats
        let price = coin.currentPrice.toCurrency()
        let pricePercentChange = coin.priceChangePercentage24H
        let priceStats = StatisticModel(title: "Current Price",
                                        value: price,
                                        percentageChange: pricePercentChange)
        
        //        market stats
        let marketCap = coin.marketCap
        let marketCapPercentChange = coin.marketCapChangePercentage24H
        let marketCapStat = StatisticModel(title: "Market Capitalization",
                                           value: "\(marketCap)",
                                           percentageChange: marketCapPercentChange)
        
        //        rank stats
        let rank = coin.marketCapRank
        let rankStat = StatisticModel(title: "Rank",
                                      value: "\(rank)",
                                      percentageChange: nil)
        
        //        volume stats
        let volume = coin.totalVolume
        let volumeStat = StatisticModel(title: "Volume",
                                        value: "\(volume)",
                                        percentageChange: nil)
        
        return CoinDetailSectionModel(title: "OverView", stats: [priceStats,
                                                                 marketCapStat,
                                                                 rankStat,
                                                                 volumeStat])
    }
    
    var additionalDetailsSectionModel: CoinDetailSectionModel {
        //        24H high
        let high = coin.high24H?.toCurrency() ?? "n/a"
        let highStat = StatisticModel(title: "24H High",
                                      value: high,
                                      percentageChange: nil)
        
        //        24H low
        let low = coin.low24H?.toCurrency() ?? "n/a"
        let lowStat = StatisticModel(title: "24H Low",
                                      value: low,
                                      percentageChange: nil)
        
        //        24H price change
        let priceChange24 = coin.priceChange24H.toCurrency()
        let percentChange24 = coin.priceChangePercentage24H
        let priceChangeStat =  StatisticModel(title: "24H Price Change",
                                              value: priceChange24,
                                              percentageChange: percentChange24)
        
        //        24H market cap change
        let marketCapChange = coin.marketCapChange24H?.toCurrency()
        let marketCapChangePercent = coin.marketCapChangePercentage24H
        let marketStat = StatisticModel(title: "24H Market Cap Change",
                                        value: "\(marketCapChange)",
                                        percentageChange: marketCapChangePercent)
        
        return CoinDetailSectionModel(title: "Additional Details", stats: [highStat,
                                                                           lowStat,
                                                                           priceChangeStat,
                                                                           marketStat])
    }
    
    init(coin: Coin) {
        self.coin = coin
    }
}
